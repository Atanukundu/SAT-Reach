/*
 * Post_Assignment.cpp
 *
 *  Created on: 24-Nov-2014
 *      Author: amit
 */

#include <application/sfUtility.h>
#include <utilities/postAssignment.h>


/*
 * Assignment to a constraint polyhedron newPolytope
 * Post_assign(newPolytope) = R x newPolytope + W
 * where W is a constant vector and R is the transition dynamics
 * The mapped states are computed exactly by mapping the polyhedron
 * Post_assign(newPolytope) = {x e Rn | Ap R^-1 x <= bp + Ap R^-1 w}   w e W
 *
 */
polytope::ptr post_assign_exact(polytope::ptr newPolytope, math::matrix<double> R,
		std::vector<double> w) {
	math::matrix<double> AA, A_dash;
	math::matrix<double> R_inverse(R.size1(), R.size2());
	std::vector<double> b_p, b_dash, term2;
	bool invertible;

    //polytope post_assign;
	b_p = newPolytope->getColumnVector();

	invertible = R.inverse(R_inverse);//Size of R_inverse has to be assigned otherwise error

	if (invertible) {

		AA = newPolytope->getCoeffMatrix();
		A_dash = AA.multiply(R_inverse);

		A_dash.mult_vector(w, term2);
		b_dash = vector_add(b_p, term2);

	} else{
		std::cout << "\nThe Transition Dynamics Matrix is not Invertible!!!\n";
	}

	return polytope::ptr(new polytope(A_dash, b_dash, 1));
}
/*
 * Computing Transition Successors with Support Function
 * Recall proposition of support function with matrix and convex set in any directions
 *
 */

polytope::ptr post_assign_approx(polytope::ptr newPolytope, math::matrix<double> R,
		polytope W, math::matrix<double> Directions, int lp_solver_type) {
	math::matrix<double> R_transpose;
	int max_or_min = 2;	//Maximizing
	std::vector<double> b(Directions.size1()), each_direction(
			Directions.size2()), direction_trans;
	R_transpose = R.transpose();
	//create glpk object to be used by the polytope
	int type=lp_solver_type;
	lp_solver lp(type);
	lp.setMin_Or_Max(max_or_min);
	lp.setConstraints(newPolytope->getCoeffMatrix(),newPolytope->getColumnVector(),newPolytope->getInEqualitySign());

	for (unsigned int i = 0; i < Directions.size1(); i++) {
		for (unsigned int j = 0; j < Directions.size2(); j++)
			each_direction[j] = Directions(i, j);
		R_transpose.mult_vector(each_direction, direction_trans);

		b[i] = newPolytope->computeSupportFunction(direction_trans, lp) + W.computeSupportFunction(each_direction, lp);
	}

	return polytope::ptr(new polytope(Directions, b, 1));
}



/*
 * Computing Transition Successors with Support Function
 * Recall proposition of support function with matrix and convex set in any directions
 *
 * This function assumes a vector w given in the transition assignment. Hence, deterministic.
 * i.e. X' = Rx + w
 */

polytope::ptr post_assign_approx_deterministic(polytope::ptr newPolytope, math::matrix<double> R,
		std::vector<double> w, math::matrix<double> Directions, int lp_solver_type) {
	math::matrix<double> R_transpose;
	int max_or_min = 2;	//Maximizing
	std::vector<double> b(Directions.size1()), each_direction(Directions.size2()), direction_trans;
	/*std::cout<<"newPolytope->getCoeffMatrix()="<<newPolytope->getCoeffMatrix()<<"\n columnVector are";
	for (int i=0;i<newPolytope->getColumnVector().size();i++)
		std::cout<<newPolytope->getColumnVector()[i]<<"\n";*/

	R_transpose = R.transpose();
	//create glpk object to be used by the polytope
	int type=lp_solver_type;
	lp_solver lp(type);
	lp.setMin_Or_Max(max_or_min);
	lp.setConstraints(newPolytope->getCoeffMatrix(),newPolytope->getColumnVector(),newPolytope->getInEqualitySign());
	for (unsigned int i = 0; i < Directions.size1(); i++) {
		for (unsigned int j = 0; j < Directions.size2(); j++)
			each_direction[j] = Directions(i, j);
		R_transpose.mult_vector(each_direction, direction_trans);
		try{
			b[i] = newPolytope->computeSupportFunction(direction_trans, lp) + dot_product(each_direction, w);
		} catch(...){
			b[i] = 999; // a large value set as the support function since the solution is unbounded
		}
	}

	/*std::cout<<"\nnewShiftedPolytope->getCoeffMatrix()="<<Directions<<"\n columnVector are";
		for (int i=0;i<b.size();i++)
			std::cout<<b[i]<<"\n";*/

	return polytope::ptr(new polytope(Directions, b, 1));
}
